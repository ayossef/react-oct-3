# Adding Redux Support to a project

## Project Wide Actions
### Installing required packages

```bash
npm i redux react-redux @reduxjs/toolkit
```

### Define the store
Store is simply built as a group of states.

### Provide the store
By wrapping the <APP> with <Provider store={}>

## State Related Actions
### Define a state inside the store
By Creating "SLICE"
What is a slice?
It is an object containing:
- State Name
- State structure and initialValue
- Actions for that state

## Component Related Actions
### Define the read access
By calling the useSelector hook

### Define the update access
- By calling the useDispatch hook -> gives an access to a Queue
- dispatch the required action -> submit the action to the queue