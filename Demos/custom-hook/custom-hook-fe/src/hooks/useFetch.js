import { useState, useEffect } from "react"

export const useFetch = (url) => { 
    const [data, setData] = useState([]);
    useEffect(() => {
        fetch(url)
        .then((resp) => resp.json() )
        .then((jsonResp) =>  setData(jsonResp) )
    }, []);
    return data
 }