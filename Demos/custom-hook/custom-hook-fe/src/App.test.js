import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText('Users');
  expect(linkElement).toBeInTheDocument();
});


test('Dummy Test', () => {
  // AAA
  // Arrange
  let x = 10
  let y = 20

  // Act
  let z = x + y

  // Assert
  expect(z).toBe(30)
});
