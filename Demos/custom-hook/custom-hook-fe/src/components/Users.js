import React from 'react'
import { useState, useEffect } from 'react';

function Users() {
    var ready = false
    const url = "http://localhost:3000/users"
    const [usersList, setUsersList] = useState([]);
    useEffect(() => {
      fetch(url)
      .then((resp) =>  resp.json())
      .then((jsonResp) =>  {ready = true; setUsersList(jsonResp)})
    }, []);
    console.log(usersList)
  return (
    <div>
        <h2>Users</h2>
        <div>{usersList.map((user) => 
        <div>
           <p>{user.id} - {user.name}</p>
          <p>{user.email}</p>
        </div>
         )}</div>
    </div>
  )
}

export default Users