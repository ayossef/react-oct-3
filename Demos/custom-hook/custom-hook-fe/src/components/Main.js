import React from 'react'
import Users from './Users'
import Products from './Products'

function Main() {
  return (
    <div>
        <h1>
        Main
        </h1>
        <Users/>
        <Products/>
    </div>
  )
}

export default Main