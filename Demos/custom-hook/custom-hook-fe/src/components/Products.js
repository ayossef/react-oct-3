import React from 'react'
import { useFetch } from '../hooks/useFetch'

function Products() {
  const url = "http://localhost:3000/products"
  var products = useFetch(url)
  return (
    <div>
        <h2>Products</h2>
        {products.map((product) =>  <div>
          <p>{product.name} - {product.price}$</p>
        </div> )}
    </div>
  )
}

export default Products