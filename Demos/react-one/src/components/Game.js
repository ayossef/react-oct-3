import React, { useState } from 'react'

function Game(props) {
    const [userIput, setUserIput] = useState('');
    const [message, setMessage] = useState('Try again')
    const magicNumber = props.magicNumerFromParent
    const checkAction = () => { 
        console.log('Check has been called')
        console.log('Message now is equal to = '+message)
        console.log('Value of user input is '+userIput)
        console.log('Value of magicNumerFromParent = '+magicNumber)
        if(Number(userIput) === magicNumber){
            setMessage("Congraulations")
        }else{
            setMessage("Try again")
        }
     }
  return (
    <div>
        <h1>Game</h1>
        <input onChange={(event) => { setUserIput(event.target.value) }} placeholder='Your Guess Here ..'></input><br></br>
        <button onClick={checkAction}>Feeling Lucky</button>
        <p>{message}</p>
    </div>
  )
}

export default Game