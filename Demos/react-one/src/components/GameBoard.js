import React from 'react'
import Game from './Game'
import { useState } from 'react'
function GameBoard() {
    const [magicNumber, setMagicNumber] = useState(0)
    const newGameAction = () => { 
        setMagicNumber(Math.round(Math.random()*10))
        console.log('Magic Number Value is '+magicNumber)
     }
  return (
    <div>
        <h1>
            GameBoard
        </h1>
        <button onClick={newGameAction}> New Game </button>
        <Game magicNumerFromParent={magicNumber}></Game>
    </div>
  )
}

export default GameBoard