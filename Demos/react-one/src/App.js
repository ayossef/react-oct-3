import './App.css';
import Game from './components/Game';
import GameBoard from './components/GameBoard';

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <GameBoard></GameBoard>
      </header>
    </div>
  );
}

export default App;
