import React from 'react'
import { MainContext } from './Main';
import {useContext} from 'react';
function FourComp() {
    const store = useContext(MainContext);
  return (
    <div>
        <h4>FourComp</h4>
        <p>{store.msg}</p>
    </div>
  )
}

export default FourComp