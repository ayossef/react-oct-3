import React from 'react'
import OneComp from './OneComp'
import {useState, createContext} from 'react'

export const MainContext = createContext()

function Main() {
    const [userMsg, setUserMsg] = useState('')
    const store = {
        msg: userMsg
    }
  return (
   <MainContext.Provider value={store}>
     <div>
        <h1>Main</h1>
        <input placeholder='Enter your message' 
        onChange={(event) => { setUserMsg(event.target.value) }}> 
        </input>
        <p>{store.msg}</p>
        <OneComp></OneComp>
    </div>
   </MainContext.Provider>
  )
}

export default Main