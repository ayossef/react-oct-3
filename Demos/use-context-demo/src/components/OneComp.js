import React from 'react'
import TwoComp from './TwoComp'
import ThreeComp from './ThreeComp'

function OneComp() {
  return (
    <div>
        <h2>
        OneComp
        </h2>
        <TwoComp/>
        <ThreeComp/>
    </div>
  )
}

export default OneComp