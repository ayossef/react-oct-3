import {createSlice} from '@reduxjs/toolkit'
export const scoreSlice = createSlice({
    name: 'score',
    initialState:{
        value: 0
    },
    reducers:{
        incScore: (state) => { state.value = state.value + 1 },
        decScore: (state) => { state.value = state.value - 1 },
        clearScore: (state) => { state.value = 0 },
        setScore: (state, action) => { state.value =  action.payload.newScore }
    }
})

export const {incScore, decScore, clearScore, setScore} = scoreSlice.actions
export default scoreSlice.reducer