import { combineReducers } from "redux";
import scoreSlice from "./scoreSlice";
import usernameSlice from "./usernameSlice";
import { configureStore } from "@reduxjs/toolkit";
let reducer = combineReducers({
    score: scoreSlice,
    username: usernameSlice
})

export default configureStore({
    reducer
})