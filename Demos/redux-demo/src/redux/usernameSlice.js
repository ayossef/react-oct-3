import { createSlice } from "@reduxjs/toolkit";

export const usernameSlice = createSlice({
    name: 'username',
    initialState:{
        value: 'N/A'
    },
    reducers:{
        setUsername: (state, action) => { state.value = action.payload.username },
        clearUsername: (state) => { state.value = '' }
    }
})

export const {setUsername, clearUsername} = usernameSlice.actions
export default usernameSlice.reducer