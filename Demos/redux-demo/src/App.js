import logo from './logo.svg';
import './App.css';
import { useSelector } from 'react-redux';
import Main from './components/Main';
import Reset from './components/Reset';

function App() {
  const score = useSelector((state) => state.score.value )
  const username = useSelector((state) => state.username.value)
  return (
    <div className="App">
      <header className="App-header">
        <p>Current Score Value: {score} </p>
        <p>Current username Value: {username} </p>
        <Main></Main>
        <Reset></Reset>
      </header>
    </div>
  );
}

export default App;
