import React from 'react'
import Play from './Play'
import Display from './Dispaly'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { setUsername } from '../redux/usernameSlice'
import { clearScore } from '../redux/scoreSlice'

function Main() {
  const [localUsername, setLocalUsername] = useState('')
  const dispatchQ = useDispatch()
  const startGameAction = () => { 
    dispatchQ(setUsername({username: localUsername}))
    dispatchQ(clearScore())
   }
  return (
    <div>
      <h1>Main</h1>
      <input placeholder='Enter your username' onChange={(event) => { setLocalUsername(event.target.value) }}></input>
      <button onClick={startGameAction}>Start Game</button>
      <Play></Play>
      <Display></Display>
    </div>
  )
}

export default Main