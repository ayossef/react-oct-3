import React from 'react'
import { useSelector } from 'react-redux'

function Dispaly() {
  const username = useSelector((state) => { return state.username.value })
  const score = useSelector((state) => { return state.score.value })
  return (
    <div>
      <h2>Dispaly</h2>
      <p> User: {username} has got the score of {score}</p>
    </div>
  )
}
export default Dispaly