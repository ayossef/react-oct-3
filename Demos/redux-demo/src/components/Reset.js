import React from 'react'
import { clearScore } from '../redux/scoreSlice'
import { clearUsername } from '../redux/usernameSlice'
import { useDispatch } from 'react-redux'
function Reset() {
  const dispatchQ = useDispatch()
  const resetAll = () => { 
    dispatchQ(clearScore())
    dispatchQ(clearUsername())
   }
  return (
    <div>
      <h1>
      Reset
      </h1>
      <button onClick={resetAll}>Reset All</button>
    </div>
  )
}

export default Reset