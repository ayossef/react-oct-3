import React from 'react'
import { useDispatch } from 'react-redux'
import { decScore, incScore } from '../redux/scoreSlice'

function Play() {
  const dispatchQ = useDispatch()
  const add = () => { dispatchQ(incScore()) }
  const sub = () => { dispatchQ(decScore()) }
  return (
    <div>
      <h2>Play</h2>
      <button onClick={add}>Increament Score</button>
      <button onClick={sub}>Decreament Score</button>
    </div>
  )
}

export default Play