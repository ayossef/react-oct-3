import logo from './logo.svg';
import './App.css';
import CountersLand from './components/CountersLand';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CountersLand></CountersLand>
      </header>
    </div>
  );
}

export default App;
