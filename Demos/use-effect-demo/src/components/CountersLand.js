import React from 'react'
import {useState, useEffect} from 'react'
function CountersLand() {
    const [counterOne, setCounterOne] = useState(0)
    const [counterTwo, setCounterTwo] = useState(0)
    const [counterThree, setCounterThree] = useState(0)
    const [renderTimes, setrenderTimes] = useState(0);
    const [mountTimes, setMountTimes] = useState(0);
    const [renderTimesBasedOnCounterOne, setrenderTimesBasedOnCounterOne] = useState(0)
    const incCounterOne = () => { setCounterOne(counterOne+1) }
    const incCounterTwo = () => { setCounterTwo(counterTwo+1) }
    const incCounterThree = () => { setCounterThree(counterThree+1) }
    useEffect(() => {
      setMountTimes(mountTimes+1)
    }, [])
    
    useEffect(() => {
      setrenderTimes(renderTimes+1)
    }, [counterOne, counterTwo, counterThree])
    
    useEffect(() => {
        setrenderTimesBasedOnCounterOne(renderTimesBasedOnCounterOne+1)
      }, [counterOne])

  return (
    <div>
        <h1>CountersLand</h1>
        <h3>{counterOne}</h3>
        <button onClick={incCounterOne}>Update Counter One</button>
        <h3>{counterTwo}</h3>
        <button onClick={incCounterTwo}>Update Counter One</button>
        <h3>{counterThree}</h3>
        <button onClick={incCounterThree}>Update Counter One</button>
        <h2>This component has been mounted {mountTimes}</h2>
        <h2>This component has been rerndered {renderTimes}</h2>
        <h2>This component has been rerndered {renderTimesBasedOnCounterOne}</h2>    </div>
  )
}

export default CountersLand